export const environment = {
  production: true,
  baseUrl: 'https://penman-application.herokuapp.com/api'
};
