import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {UserService} from '../services/user-service';
import {Role} from '../entities/role.enum';

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {

  private isAdmin = false;

  constructor(private userService: UserService,
              private router: Router) {
    this.userService.getCurrentUser().subscribe(currentUser => this.isAdmin = currentUser.role === Role.ADMIN);
  }

  canActivate(route: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (!this.isAdmin) {
      this.router.navigate(['login']);
      return false;
    }
    return true;
  }
}
