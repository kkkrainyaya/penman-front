import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AppComponent} from './app.component';
import {EmailConfirmComponent} from './components/email-confirm-component/email-confirm.component';
import {LoginComponent} from './components/login/login.component';
import {ProfileComponent} from './components/profile/profile.component';
import {StoryDescriptionComponent} from './components/story-description/story-description.component';
import {StoryEditingComponent} from './components/story-editing/story-editing.component';
import {AdminComponent} from './components/admin/admin.component';
import {StoryReadingComponent} from './components/story-reading/story-reading.component';
import {ChapterReadingComponent} from './components/chapter-reading/chapter-reading.component';
import {HomeComponent} from './components/home/home.component';
import {AuthenticatedUserGuard} from './guards/authenticated-user.guard';
import {SearchComponent} from './components/search/search.component';
import {PageNotFoundComponent} from './components/page-not-found/page-not-found';
import {AdminGuard} from './guards/admin.guard';


const routes: Routes = [
  {
    path: 'stories/new',
    component: StoryDescriptionComponent,
    canActivate: [AuthenticatedUserGuard]
  },
  {
    path: 'stories/:storyId/edit',
    component: StoryDescriptionComponent,
    canActivate: [AuthenticatedUserGuard]
  },
  {
    path: 'stories/:storyId/chapters/:chapterId/edit',
    component: StoryEditingComponent,
    canActivate: [AuthenticatedUserGuard]
  },
  {
    path: 'confirm',
    component: EmailConfirmComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'stories/:storyId',
    component: StoryReadingComponent
  },
  {
    path: 'stories/:storyId/chapters/:chapterId',
    component: ChapterReadingComponent
  },
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'profile/:id',
    component: ProfileComponent
  },
  {
    path: 'search',
    component: SearchComponent,
    runGuardsAndResolvers: 'always'
  },
  {
    path: 'admin',
    component: AdminComponent,
    canActivate: [AdminGuard]
  },
  {
    path: 'page-not-found',
    component: PageNotFoundComponent
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
