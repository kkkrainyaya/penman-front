import {Component, OnInit} from '@angular/core';
import {Story} from '../../entities/story';
import {StoryService} from '../../services/story-service';
import {ActivatedRoute} from '@angular/router';
import {Genre} from '../../entities/genre';

const FIRST_PAGE = 0;
const SIZE = 8;
const DIR_DESC = 'desc';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  searchValue: string;
  result: Story[];
  isFullTextSearch = false;
  genre: Genre = new Genre();
  isLastPage = true;
  currentPage: number;
  nextPage: number;
  stories: Story[] = [];
  filter: string;

  constructor(private storyService: StoryService,
              private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.currentPage = FIRST_PAGE;
    this.route.queryParams.subscribe(params => {
      this.stories = [];
      if (params.tag) {
        this.filter = params.tag;
        this.getByTag(params.tag, FIRST_PAGE);
      } else if (params.genre) {
        this.filter = params.genre;
        this.getByGenre(params.genre, FIRST_PAGE);
      } else if (params.filter) {
        params.filter === 'rating' ? this.filter = 'Популярные' : this.filter = 'Последние обновления';
        this.getSorted(params.filter, FIRST_PAGE);
      } else {
        this.isFullTextSearch = true;
      }
    });
  }

  private getByGenre(genre: string, currentPage: number) {
    this.genre = JSON.parse(window.localStorage.getItem('genres')).filter(el => el.name === genre).pop();
    this.storyService.getStories(currentPage, SIZE, null, this.genre.id.toString()).subscribe(
      data => {
        this.result = data.content;
        this.result.forEach(story => this.stories.push(story));
        this.isLastPage = data.last;
        this.currentPage = data.number;
      }
    );
  }

  private getByTag(tag: string, currentPage: number) {
    this.storyService.getStories(currentPage, SIZE, tag, null).subscribe(
      data => {
        this.result = data.content;
        this.result.forEach(story => this.stories.push(story));
        this.isLastPage = data.last;
        this.currentPage = data.number;
      }
    );
  }

  private getSorted(filter: string, currentPage: number) {
    this.storyService.getStoriesSorted(filter, currentPage, SIZE, DIR_DESC).subscribe(
      data => {
        this.result = data.content;
        this.result.forEach(story => this.stories.push(story));
        this.isLastPage = data.last;
        this.currentPage = data.number;
      }
    );
  }

  getNextPage() {
    this.nextPage = this.currentPage + 1;
    this.route.queryParams.subscribe(params => {
      if (params.tag) {
        this.getByTag(params.tag, this.nextPage);
      } else if (params.genre) {
        this.getByGenre(params.genre, this.nextPage);
      } else if (params.filter) {
        this.getSorted(params.filter, this.nextPage);
      } else {
        this.isFullTextSearch = true;
      }
    });
  }
}
