import {Component, Input, OnInit} from '@angular/core';
import {Comment} from '../../entities/comment';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import {User} from '../../entities/user';
import {Story} from '../../entities/story';
import {environment} from '../../../environments/environment';
import {UserService} from '../../services/user-service';

const API_URL = environment.baseUrl;

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss']
})
export class CommentsComponent implements OnInit {

  @Input() story: Story;
  comment: Comment = new Comment();
  private stompClient;
  currentUser: User;

  constructor(private userService: UserService) {
    this.userService.getCurrentUser().subscribe(currentUser => this.currentUser = currentUser);
  }

  ngOnInit(): void {
    this.initializeWebSocketConnection();
  }

  initializeWebSocketConnection() {
    const ws = new SockJS(API_URL + '/v1/jsa-stomp-endpoint');
    this.stompClient = Stomp.over(ws);
    const that = this;
    // tslint:disable-next-line:only-arrow-functions
    this.stompClient.connect({}, function(frame) {
      that.stompClient.subscribe(`/stories/${that.story.id}/comments`, (comment) => {
        if (comment.body) {
          that.story.comments.push(JSON.parse(comment.body));
        }
      });
    });
  }

  sendComment() {
    this.stompClient.send(`/jsa/stories/${this.story.id}/comments`,
      {Authorization: 'Bearer ' + this.currentUser.token}, JSON.stringify(this.comment));
    this.clearComment();
  }

  clearComment() {
    this.comment.content = '';
  }
}
