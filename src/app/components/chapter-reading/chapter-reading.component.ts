import {Component, OnInit} from '@angular/core';
import {Story} from '../../entities/story';
import {ActivatedRoute, Router} from '@angular/router';
import {User} from '../../entities/user';
import {StoryService} from '../../services/story-service';
import {Chapter} from '../../entities/chapter';
import {UserService} from '../../services/user-service';

@Component({
  selector: 'app-chapter-reading',
  templateUrl: './chapter-reading.component.html',
  styleUrls: ['./chapter-reading.component.scss']
})
export class ChapterReadingComponent implements OnInit {
  story: Story;
  chapter: Chapter;
  chapterId: number;
  storyId: number;
  isLastChapter = false;
  isPreview = false;
  isLiked = false;
  currentUser: User;

  constructor(private route: ActivatedRoute,
              private storyService: StoryService,
              private userService: UserService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.userService.getCurrentUser().subscribe(user => this.currentUser = user);

    this.route.queryParams.subscribe(params => {
      if (params.preview === 'true') {
        this.isPreview = true;
      }
    });
    this.route.params.subscribe(params => {
      console.log(params);
      this.chapterId = +params.chapterId;
      this.storyId = +params.storyId;
      this.getStory();
    });
  }

  getStory() {
    if (this.isPreview) {
      this.chapter = JSON.parse(window.localStorage.getItem('chapter' + this.chapterId));
    } else {
      this.story = JSON.parse(window.localStorage.getItem('story' + this.storyId));
      if (!this.story) {
        this.storyService.getStory(this.storyId).subscribe(
          data => {
            this.story = data;
            window.localStorage.setItem('story' + this.story.id, JSON.stringify(this.story));
            this.setChapter();
          }
        );
      } else {
        this.setChapter();
      }
    }
  }

  setChapter() {
    this.story.chapters = this.story.chapters.sort(
      (chapter1, chapter2) => chapter1.number - chapter2.number);
    this.chapter = this.story.chapters.filter(chapter => chapter.id === this.chapterId).pop();
    this.isLast();
    this.isLiked = this.currentUser && this.chapter.likedUserIds.includes(this.currentUser.id);
  }

  showNext() {
    this.router.navigate(['/stories', this.story.id, 'chapters',
      this.story.chapters.filter(chapter => chapter.number - 1 === this.chapter.number).pop().id]);
  }

  isLast() {
    if (this.chapter.number === this.story.chapters.length) {
      this.isLastChapter = true;
    }
  }

  goToEdit() {
    this.router.navigate(['/stories', this.storyId, 'chapters', this.chapterId, 'edit']);
  }

  like() {
    if (!this.isLiked && this.currentUser) {
      this.storyService.addLike(this.story.id, this.chapter.id).subscribe(response => {
        this.isLiked = true;
        this.chapter.likedUserIds.push(this.currentUser.id);
      });
    }
  }
}
