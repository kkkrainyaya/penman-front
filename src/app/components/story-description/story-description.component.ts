import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {NgxSpinnerService} from 'ngx-spinner';
import {Observable} from 'rxjs';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {MatAutocomplete, MatAutocompleteSelectedEvent} from '@angular/material/autocomplete';
import {MatChipInputEvent} from '@angular/material/chips';
import {map, startWith} from 'rxjs/operators';
import {Genre} from '../../entities/genre';
import {StoryService} from '../../services/story-service';
import {Locale} from '../../entities/locale.enum';
import {ActivatedRoute, Router} from '@angular/router';
import {Story} from '../../entities/story';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';


@Component({
  selector: 'app-story-description',
  templateUrl: './story-description.component.html',
  styleUrls: ['./story-description.component.scss']
})
export class StoryDescriptionComponent implements OnInit {

  tags: string[] = [];
  allTags: string[] = [];
  selectable = true;
  removable = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  tagCtrl = new FormControl();
  filteredTags: Observable<string[]>;
  genres: Genre[];
  story: Story = new Story();

  @ViewChild('tagInput') tagInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto') matAutocomplete: MatAutocomplete;
  storyForm: FormGroup;
  submitted = false;
  isExisted = true;
  isUploadWindowActive = false;

  constructor(private storyService: StoryService,
              private router: Router,
              private route: ActivatedRoute,
              private formBuilder: FormBuilder,
              private spinner: NgxSpinnerService) {
    storyService.getAllGenres(Locale[Locale.RU])
      .subscribe(data => this.genres = data);
    storyService.getAllTags()
      .subscribe(data => {
        this.allTags = data;
      });
    this.filteredTags = this.tagCtrl.valueChanges.pipe(
      startWith(null),
      map((title: string | null) => title ?
        this._filter(title) : this.allTags.slice()));

    this.route.url.subscribe(url => {
      if (url[1].path.includes('new')) {
        this.isExisted = false;
        this.story.imageId = 'default-image';
      } else {
        this.storyService.getStory(Number.parseInt(url[1].path, 10)).subscribe(
          data => {
            this.story = data;
            this.story.chapters = this.story.chapters.sort(
              (chapter1, chapter2) => chapter1.number - chapter2.number);
            this.tags = this.story.tags;
            this.story.genre = this.genres.filter(genre => genre.id = this.story.genre.id).pop();
            window.localStorage.setItem(this.story.id.toString(), this.story.title);
          }
        );
      }
    });
  }

  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;
    if ((value || '').trim()) {
      this.tags.push(value.trim());
    }
    if (input) {
      input.value = '';
    }
    this.tagCtrl.setValue(null);
  }

  remove(tag: string): void {
    const index = this.tags.indexOf(tag);

    if (index >= 0) {
      this.tags.splice(index, 1);
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.tags.push(event.option.viewValue);
    this.tagInput.nativeElement.value = '';
    this.tagCtrl.setValue(null);
  }

  private _filter(value: string): string[] {
    const filterValue = value;
    return this.allTags.filter(tag =>
      tag.toLowerCase().indexOf(filterValue) === 0);
  }

  onSubmit() {
    this.submitted = true;
    if (this.storyForm.invalid) {
      return;
    }
    this.story.tags = this.tags;
    if (this.isExisted === false) {
      this.storyService.addStory(this.story).subscribe(
        data => {
          this.story = data;
          window.localStorage.setItem('story' + this.story.id, JSON.stringify(this.story));
          this.router.navigate(['/stories', data.id, 'chapters', data.chapters[0].id, 'edit']);
        }
      );
    } else {
      this.story.chapters.forEach(chapter => chapter.storyId = this.story.id);
      this.storyService.updateStory(this.story).subscribe(
        data => console.log(data)
      );
    }
  }

  ngOnInit(): void {
    this.storyForm = this.formBuilder.group({
      title: ['', Validators.required],
      genre: ['', Validators.required],
      description: ['']
    });
  }

  get f() {
    return this.storyForm.controls;
  }

  addChapter() {
    this.storyService.addChapter(this.story.id).subscribe(
      data => {
        this.story.chapters.push(data);
        this.router.navigate(['/stories', this.story.id, 'chapters', data.id, 'edit']);
      }
    );
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.story.chapters, event.previousIndex, event.currentIndex);
    this.story.chapters = [...this.story.chapters];
    this.story.chapters.forEach(chapter => chapter.number = this.story.chapters.indexOf(chapter) + 1);
    this.story.chapters = this.story.chapters.sort(
      (chapter1, chapter2) => chapter1.number - chapter2.number);
  }

  deleteChapter(id: number) {
    this.storyService.deleteChapter(this.story.id, id).subscribe(
      res => this.story.chapters = this.story.chapters.filter(chapter => chapter.id !== id)
    );
  }

  read() {
    this.router.navigate(['/stories', this.story.id]);
  }

  changeStoryImage(): void {
    this.isUploadWindowActive = true;
  }

  closeUploadWindow(): void {
    this.isUploadWindowActive = false;
  }

  isFileUploaded(event): void {
    this.story.imageId = event;
    window.localStorage.setItem('story' + this.story.id, JSON.stringify(this.story));
    this.spinner.hide();
  }

  isFileDropped(): void {
    this.closeUploadWindow();
    this.spinner.show();
  }
}
