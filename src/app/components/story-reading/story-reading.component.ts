import {Component, OnInit} from '@angular/core';
import {Story} from '../../entities/story';
import {ActivatedRoute} from '@angular/router';
import {User} from '../../entities/user';
import {StoryService} from '../../services/story-service';
import {UserService} from '../../services/user-service';

@Component({
  selector: 'app-story-reading',
  templateUrl: './story-reading.component.html',
  styleUrls: ['./story-reading.component.scss']
})
export class StoryReadingComponent implements OnInit {

  story: Story;
  isVoted = false;
  currentUser: User;

  constructor(private route: ActivatedRoute,
              private storyService: StoryService,
              private userService: UserService) {
    userService.getCurrentUser().subscribe(user => this.currentUser = user);
  }

  ngOnInit(): void {
    this.getStory();
  }

  getStory() {
    this.route.params.subscribe(params => {
      this.storyService.getStory(+params.storyId).subscribe(
        data => {
          this.story = data;
          this.story.chapters = this.story.chapters.sort(
            (chapter1, chapter2) => chapter1.number - chapter2.number);
          this.isVoted = !this.currentUser || this.story.votedUsers.includes(this.currentUser.id);
          window.localStorage.setItem('story' + this.story.id, JSON.stringify(this.story));
        }
      );
    });
  }

  updateRating($event: number) {
    this.storyService.addRating($event, this.story.id).subscribe(
      data => {
        this.story.rating = data;
        this.story.votedUsers.push(this.story.user.id);
        this.isVoted = this.story.votedUsers.includes(this.currentUser.id);
      }
    );
  }
}
