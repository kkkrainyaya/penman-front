import {HttpClient} from '@angular/common/http';
import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FileSystemFileEntry, NgxFileDropEntry} from 'ngx-file-drop';

@Component({
  selector: 'app-file-drop-upload',
  templateUrl: './file-drop.component.html',
  styleUrls: ['./file-drop.component.scss'],
})
export class FileDropComponent {
  @Input() isUploadWindowActive: boolean;
  @Output() uploadedFileEvent = new EventEmitter<string>();
  @Output() fileDroppedEvent = new EventEmitter();
  @Output() onCloseModalEvent = new EventEmitter();
  private droppedFile: NgxFileDropEntry;
  private cloudinaryConfig = {headers: {'X-Requested-With': 'XMLHttpRequest'}};
  private cloudinaryUrl = 'https://api.cloudinary.com/v1_1/profunding/image/upload';

  constructor(private http: HttpClient) {
  }

  public dropped(droppedFiles: NgxFileDropEntry[]) {
    this.droppedFile = droppedFiles[0];
    console.log(this.droppedFile);
    if (this.droppedFile.fileEntry.isFile) {
      const fileEntry = this.droppedFile.fileEntry as FileSystemFileEntry;
      fileEntry.file(file => this.uploadFile(file));
    }
  }

  private uploadFile(file: File) {
    this.fileDroppedEvent.emit();
    const formData = new FormData();
    formData.append('upload_preset', 'profunding');
    formData.append('file', file);

    return new Promise((resolve, reject) => {
      this.http.post(this.cloudinaryUrl, formData, this.cloudinaryConfig).subscribe( (response: CloudinaryResponse) => {
        console.log(response);
        this.uploadedFileEvent.emit(response.public_id);
      });
    });
  }

  closeWindow(): void {
    this.onCloseModalEvent.emit();
  }
}

class CloudinaryResponse {
  // tslint:disable-next-line:variable-name
  public_id: string;
}
