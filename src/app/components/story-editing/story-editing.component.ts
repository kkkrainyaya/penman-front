import {Component, OnInit} from '@angular/core';
import {HtmlEditorService, ImageService, LinkService, ToolbarService} from '@syncfusion/ej2-angular-richtexteditor';
import {NgxSpinnerService} from 'ngx-spinner';
import {Chapter} from '../../entities/chapter';
import {ActivatedRoute, Router} from '@angular/router';
import {Story} from '../../entities/story';
import {StoryService} from '../../services/story-service';

@Component({
  selector: 'app-story-editing',
  templateUrl: './story-editing.component.html',
  styleUrls: ['./story-editing.component.scss'],
  providers: [ToolbarService, LinkService, ImageService, HtmlEditorService]
})
export class StoryEditingComponent implements OnInit {

  public tools: object = {
    items: ['Undo', 'Redo', '|',
      'Bold', 'Italic', 'Underline', 'StrikeThrough', '|',
      'FontSize', '|',
      'Alignments', '|', 'OrderedList', 'UnorderedList', '|',
      'SourceCode']
  };
  textEditor: any;
  chapter: Chapter = new Chapter();
  story: Story = new Story();
  storyId: number;
  chapterId: number;
  isUploadWindowActive = false;

  constructor(private route: ActivatedRoute,
              private storyService: StoryService,
              private router: Router,
              private spinner: NgxSpinnerService) {
  }


  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.storyId = +params.storyId;
      this.chapterId = +params.chapterId;
      this.chapter.content = ' ';
      this.story = JSON.parse(window.localStorage.getItem('story' + this.storyId));
      this.storyService.getChapter(this.storyId, this.chapterId).subscribe(
        data => {
          this.chapter = data;
        }
      );
    });
  }

  save() {
    this.chapter.content = this.textEditor;
    this.chapter.storyId = this.story.id;
    this.storyService.updateChapter(this.storyId, this.chapter).subscribe(
      data => {
        this.chapter = data;
        this.story.chapters.push(this.chapter);
        window.localStorage.setItem('story' + this.storyId, JSON.stringify(this.story));
      }
    );
  }

  preview() {
    this.save();
    window.localStorage.setItem('chapter' + this.chapterId, JSON.stringify(this.chapter));
    this.router.navigate(['/stories', this.storyId, 'chapters', this.chapter.id], {queryParams: {preview: 'true'}});
  }

  addChapterImage(): void {
    this.isUploadWindowActive = true;
  }

  closeUploadWindow(): void {
    this.isUploadWindowActive = false;
  }

  isFileUploaded(event): void {
    this.chapter.imageId = event;
    this.spinner.hide();
  }

  isFileDropped(): void {
    this.closeUploadWindow();
    this.spinner.show();
  }
}
