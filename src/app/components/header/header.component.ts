import {Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {Role} from '../../entities/role.enum';
import {Router} from '@angular/router';
import {User} from '../../entities/user';
import {UserService} from '../../services/user-service';
import {Locale} from '../../entities/locale.enum';
import {Genre} from '../../entities/genre';
import {StoryService} from '../../services/story-service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  isLightMode = true;
  classApplied = false;
  user: User;
  genres: Genre[];
  isAdmin = false;
  selectedLanguage: string;

  constructor(private userService: UserService,
              private router: Router,
              private storyService: StoryService,
              private translate: TranslateService) {
    this.userService.getCurrentUser().subscribe(user => {
      this.user = user;
      this.isAdmin = user && user.role === Role.ADMIN;
    });
    this.userService.language.subscribe(language => {
      this.selectedLanguage = language;
      this.translate.use(language);
    });
  }

  private static scrollFunction() {
    if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
      document.getElementById('header').style.paddingTop = '0';
    } else {
      document.getElementById('header').style.paddingTop = '20px';
    }
  }

  ngOnInit(): void {
    window.addEventListener('scroll', HeaderComponent.scrollFunction, true);
    this.storyService.getAllGenres(Locale[Locale.RU]).subscribe(
      data => {
        this.genres = data;
        window.localStorage.setItem('genres', JSON.stringify(this.genres));
      }
    );
  }

  logout() {
    this.userService.signOut();
    this.router.navigate(['']);
  }

  toggleMode() {
    this.isLightMode = !this.isLightMode;
  }

  toggleClass() {
    this.classApplied = !this.classApplied;
  }

  changeLanguage() {
    this.userService.setLanguage(this.selectedLanguage);
  }
}
