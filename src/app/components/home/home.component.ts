import {Component, OnInit} from '@angular/core';
import {StoryService} from '../../services/story-service';
import {Story} from '../../entities/story';


const PAGE = 0;
const SIZE = 4;
const FILTER_RATING = 'rating';
const FILTER_MODIFIED = 'modifiedDate';
const DIR_DESC = 'desc';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  tags: string[] = [];
  popularStories: Story[] = [];
  latestStories: Story[] = [];

  constructor(private storyService: StoryService) {
  }

  ngOnInit(): void {
    this.storyService.getAllTags().subscribe(
      data => this.tags = data
    );
    this.getPopular();
    this.getLatest();
  }

  private getPopular() {
    this.storyService.getStoriesSorted(FILTER_RATING, PAGE, SIZE, DIR_DESC).subscribe(
      data => this.popularStories = data.content
    );
  }

  private getLatest() {
    this.storyService.getStoriesSorted(FILTER_MODIFIED, PAGE, SIZE, DIR_DESC).subscribe(
      data => this.latestStories = data.content
    );
  }
}


