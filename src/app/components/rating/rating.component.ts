import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';


@Component({
  selector: 'app-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.scss']
})
export class RatingComponent implements OnInit {

  @Input() rating;
  @Input() isVoted;
  @Output() selectedRating = new EventEmitter<number>();
  starCount = 5;
  private snackBarDuration = 2000;

  constructor(private snackBar: MatSnackBar) {
  }

  ngOnInit() {
  }

  getRating(rating: number) {
    this.snackBar.open('You rated ' + rating + ' / ' + this.starCount, '', {
      duration: this.snackBarDuration
    });
    setTimeout(() => {
        this.selectedRating.emit(rating);
      },
      50
    );
    this.isVoted = true;
  }
}
