import {Component, OnInit} from '@angular/core';
import {User} from '../../entities/user';
import {UserService} from '../../services/user-service';
import {remove} from 'lodash';
import {Router} from '@angular/router';
import {Role} from '../../entities/role.enum';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  users: User[] = [];
  masterSelected: any;
  checkedUsers: number[];

  constructor(private userService: UserService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.userService.getUsers().subscribe(
      data => this.users = data
    );
  }

  isAllSelected() {
    this.masterSelected = this.users.every(item => {
      return item.isSelected;
    });
    this.getCheckedItemList();
  }

  getCheckedItemList() {
    this.checkedUsers = [];
    this.users.filter(user => user.isSelected)
      .forEach(user => this.checkedUsers.push(user.id));
    return this.checkedUsers;
  }

  delete() {
    if (this.checkedUsers.length > 0) {
      this.userService.deleteUsers(this.checkedUsers).subscribe(
        remove(this.users, user =>
          this.checkedUsers.includes(user.id)));
    }
  }

  unblock() {
    this.updateBlocked(false);
  }

  block() {
    this.updateBlocked(true);
    this.checkUser();
  }

  updateBlocked(status: boolean) {
    if (this.checkedUsers.length > 0) {
      console.log(this.checkedUsers);
      this.userService.updateUsersStatus(this.checkedUsers, status).subscribe(
        data =>
          this.users.filter(el => this.checkedUsers
            .includes(el.id))
            .forEach(el => el.blocked = status)
      );
    }
  }

  toggleCheckAll() {
    this.users.forEach(user => user.isSelected = this.masterSelected);
    this.getCheckedItemList();
  }

  setAdmin() {
    this.userService.updateUsersRole(this.checkedUsers).subscribe(
      data =>
        this.users.filter(el => this.checkedUsers
          .includes(el.id))
          .forEach(el => el.role = Role.ADMIN)
    );
  }

  private checkUser() {
    this.userService.getCurrentUser().subscribe(
      data => {
        if (this.checkedUsers.includes(data.id)) {
          this.userService.signOut();
          this.router.navigate(['/']);
        }
      });
  }
}
