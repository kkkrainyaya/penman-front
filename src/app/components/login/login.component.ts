import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {UserService} from '../../services/user-service';

@Component({
  selector: 'app-register',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  isLoginView = true;
  loginForm: any = {};
  registerForm: any = {};
  isSuccessful = false;
  errorMessage = '';
  isSignUpFailed = false;

  constructor(private userService: UserService,
              private router: Router) {
  }

  ngOnInit(): void {
  }

  onSubmitLogin() {
    this.userService.login(this.loginForm).subscribe(
      data => {
        console.log(data);
        UserService.putUserToStorage(data);
        this.userService.setUser(data);
        this.router.navigate(['']);
      },
      error1 => {
        this.isSignUpFailed = true;
        this.errorMessage = error1.error.message;
      }
    );
  }

  onSubmitRegister() {
    this.userService.register(this.registerForm).subscribe(
      data => {
        this.isSuccessful = true;
        this.router.navigate(['/confirm']);
      },
      err => {
        this.isSignUpFailed = true;
        this.errorMessage = err.error.message;
      }
    );
  }

  toggleLogin() {
    this.isLoginView = !this.isLoginView;
  }
}
