import {Component, OnInit} from '@angular/core';
import {NgxSpinnerService} from 'ngx-spinner';
import {User} from '../../entities/user';
import {UserService} from '../../services/user-service';
import {ActivatedRoute, Router} from '@angular/router';
import {ActionEventArgs} from '@syncfusion/ej2-angular-inplace-editor';
import {Story} from '../../entities/story';
import {StoryService} from '../../services/story-service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  private id: number;
  user: User;
  isUploadWindowActive = false;
  canEdit = false;
  currentUser: User;

  constructor(private spinner: NgxSpinnerService,
              private userService: UserService,
              private activateRoute: ActivatedRoute,
              private storyService: StoryService,
              private router: Router) {
    this.userService.getCurrentUser().subscribe(currentUser => this.currentUser = currentUser);
    activateRoute.params.subscribe(params => this.id = params.id);
    this.userService.getUser(this.id).subscribe(response => {
        this.user = response;

      }, err => {
        this.router.navigate(['/page-not-found']);
      }
    );
  }

  ngOnInit(): void {
    this.canEdit = this.currentUser && this.currentUser.id === +this.id;
  }

  changePhoto(): void {
    this.isUploadWindowActive = true;
  }

  closeUploadWindow(): void {
    this.isUploadWindowActive = false;
  }

  isFileUploaded(event): void {
    this.user.imageId = event;
    this.userService.updateUserInfo(this.user).subscribe(response => this.user = response);
    this.spinner.hide();
  }

  isFileDropped(): void {
    this.closeUploadWindow();
    this.spinner.show();
  }

  onUsernameChange(e: ActionEventArgs): void {
    this.user.username = e.value;
    this.userService.updateUserInfo(this.user).subscribe(response => {
      this.user = response;
      if (this.currentUser && this.currentUser.id === +this.id) {
        this.currentUser.username = this.user.username;
        this.userService.setUser(this.currentUser);
      }
    });
  }
}
