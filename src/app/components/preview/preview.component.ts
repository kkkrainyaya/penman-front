import {Component, Input, OnInit} from '@angular/core';
import {Story} from '../../entities/story';

@Component({
  selector: 'app-preview',
  templateUrl: './preview.component.html',
  styleUrls: ['./preview.component.scss']
})
export class PreviewComponent implements OnInit {

  @Input() story: Story;

  constructor() {
  }

  ngOnInit(): void {
  }

}
