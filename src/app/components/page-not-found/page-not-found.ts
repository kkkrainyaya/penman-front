import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-error-user',
  templateUrl: './page-not-found.html',
  styleUrls: ['./page-not-found.scss']
})
export class PageNotFoundComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
