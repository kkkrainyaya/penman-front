import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatChipsModule} from '@angular/material/chips';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatSelectModule} from '@angular/material/select';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {InPlaceEditorModule} from '@syncfusion/ej2-angular-inplace-editor';
import {NgxFileDropModule} from 'ngx-file-drop';
import {NgxSpinnerModule} from 'ngx-spinner';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {FileDropComponent} from './components/drag-and-drop/file-drop.component';
import {EmailConfirmComponent} from './components/email-confirm-component/email-confirm.component';
import {FooterComponent} from './components/footer/footer.component';
import {HeaderComponent} from './components/header/header.component';
import {LoginComponent} from './components/login/login.component';
import {SafeHtmlPipe} from './components/pipes/SafeHtmlPipe';
import {ProfileComponent} from './components/profile/profile.component';
import {StoryDescriptionComponent} from './components/story-description/story-description.component';
import {StoryEditingComponent} from './components/story-editing/story-editing.component';
import {RichTextEditorModule} from '@syncfusion/ej2-angular-richtexteditor';
import {MatInputModule} from '@angular/material/input';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatTabsModule} from '@angular/material/tabs';
import {StoryReadingComponent} from './components/story-reading/story-reading.component';
import {ChapterReadingComponent} from './components/chapter-reading/chapter-reading.component';
import {PreviewComponent} from './components/preview/preview.component';
import {CommentsComponent} from './components/comments/comments.component';
import {HomeComponent} from './components/home/home.component';
import {AuthenticatedUserGuard} from './guards/authenticated-user.guard';
import {AuthInterceptor} from './helpers/auth.interceptor';
import {RatingComponent} from './components/rating/rating.component';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatButtonModule} from '@angular/material/button';
import {StarRatingModule} from '@sreyaj/ng-star-rating';
import {SearchComponent} from './components/search/search.component';
import {AdminComponent} from './components/admin/admin.component';
import {PageNotFoundComponent} from './components/page-not-found/page-not-found';
import {AdminGuard} from './guards/admin.guard';
import {TranslateModule, TranslateLoader, TranslateService} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    StoryDescriptionComponent,
    LoginComponent,
    ProfileComponent,
    FileDropComponent,
    LoginComponent,
    StoryEditingComponent,
    CommentsComponent,
    StoryReadingComponent,
    ChapterReadingComponent,
    PreviewComponent,
    EmailConfirmComponent,
    RatingComponent,
    HomeComponent,
    SearchComponent,
    SafeHtmlPipe,
    AdminComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatFormFieldModule,
    MatChipsModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    MatIconModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
    HttpClientModule,
    BrowserAnimationsModule,
    InPlaceEditorModule,
    NgxFileDropModule,
    NgxSpinnerModule,
    RichTextEditorModule,
    MatInputModule,
    DragDropModule,
    MatTooltipModule,
    MatTabsModule,
    MatButtonModule,
    StarRatingModule,
    TranslateModule.forRoot({
      loader: {
        provide:  TranslateLoader,
        useFactory:  HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  providers: [AuthenticatedUserGuard, MatSnackBar, AdminGuard, TranslateService,
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule {
}
