import {Role} from './role.enum';
import {Story} from './story';

export class User {
  id: number;
  username: string;
  password: string;
  email: string;
  token: string;
  role: Role;
  imageId: string;
  blocked: boolean;
  isSelected: any;
  stories: Story[];
}
