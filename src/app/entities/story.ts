import {Chapter} from './chapter';
import {Genre} from './genre';
import {Comment} from './comment';
import {User} from './user';

export class Story {
  id: number;
  title: string;
  description: string;
  chapters: Chapter[] = [];
  genre: Genre;
  tags: string[] = [];
  imageId: string;
  comments: Comment[] = [];
  rating: number;
  user: User;
  votedUsers: number[] = [];
}
