export class Chapter {
  id: number;
  title: string;
  number: number;
  content: string;
  storyId: number;
  imageId: string;
  likedUserIds: number[];
}
