import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import {User} from '../entities/user';
import {UserService} from '../services/user-service';

const TOKEN_HEADER_KEY = 'Authorization';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  currentUser: User;

  constructor(private userService: UserService) {
    this.userService.getCurrentUser().subscribe(currentUser => this.currentUser = currentUser);
  }

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    let authReq = req;
    if (this.currentUser != null && !req.url.includes('api.cloudinary.com')) {
      authReq = req.clone({ headers: req.headers.set(TOKEN_HEADER_KEY, 'Bearer ' + this.currentUser.token) });
    }
    return next.handle(authReq);
  }
}

export const authInterceptorProviders = [
  { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }
];
