import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {Story} from '../entities/story';
import {Chapter} from '../entities/chapter';

const API_URL = environment.baseUrl + '/v1/stories';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class StoryService {

  constructor(private http: HttpClient) {
  }

  getAllGenres(locale: string): Observable<any> {
    const options = {
      headers: new HttpHeaders({'Content-Type': 'application/json'}),
      params: new HttpParams().set('locale', locale)
    };
    return this.http.get(API_URL + '/genres', options);
  }

  getAllTags(): Observable<any> {
    return this.http.get(API_URL + '/tags', httpOptions);
  }

  addStory(story: Story): Observable<any> {
    return this.http.post(API_URL, JSON.stringify(story), httpOptions);
  }

  updateStory(story: Story): Observable<any> {
    return this.http.put(API_URL + `/${story.id}`, JSON.stringify(story), httpOptions);
  }

  updateChapter(storyId: number, chapter: Chapter): Observable<any> {
    console.log(chapter);
    return this.http.put(API_URL + `/${storyId}/chapters/${chapter.id}`, JSON.stringify(chapter), httpOptions);
  }

  addChapter(storyId: number): Observable<any> {
    return this.http.post(API_URL + `/${storyId}/chapters`, httpOptions);
  }

  getStory(id: number): Observable<any> {
    return this.http.get(API_URL + `/${id}`, httpOptions);
  }

  getChapter(storyId: number, chapterId: number): Observable<any> {
    return this.http.get(API_URL + `/${storyId}/chapters/${chapterId}`, httpOptions);
  }

  deleteChapter(storyId: number, chapterId: number): Observable<any> {
    return this.http.delete(API_URL + `/${storyId}/chapters/${chapterId}`, httpOptions);
  }

  addRating(rating: number, storyId: number): Observable<any> {
    return this.http.post(API_URL + `/${storyId}/rating`, rating, httpOptions);
  }

  addLike(storyId: number, chapterId: number): Observable<any> {
    return this.http.post(API_URL + `/${storyId}/chapters/${chapterId}/likes`, null, httpOptions);
  }

  getStoriesSorted(filter: string, page: number, size: number, dir: string): Observable<any> {
    const options = {
      headers: httpOptions.headers,
      params: new HttpParams().set('filter', filter)
        .set('page', page.toString())
        .set('size', size.toString())
        .set('sort', filter.toString() + ',' + dir)
    };
    const search = {};
    return this.http.post(API_URL + '/search', search, options);
  }

  getStories(page: number, size: number, tagSearch: string, genreSearch: string): Observable<any> {
    const search = {
      tag: tagSearch,
      genreId: genreSearch
    };
    const options = {
      headers: httpOptions.headers,
      params: new HttpParams()
        .set('page', page.toString())
        .set('size', size.toString())
    };
    return this.http.post(API_URL + '/search', search, options);
  }
}
