import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {User} from '../entities/user';
import {Role} from '../entities/role.enum';

const API_URL = environment.baseUrl + '/v1/users';
const USER_KEY = 'auth-user';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private userSource = new BehaviorSubject<User>(UserService.getUserFromStorage());
  private currentUser = this.userSource.asObservable();
  private languageSource = new BehaviorSubject<string>(localStorage.getItem('language') || 'en');
  language = this.languageSource.asObservable();

  constructor(private http: HttpClient) {
  }

  public static putUserToStorage(user: User) {
    window.localStorage.removeItem(USER_KEY);
    window.localStorage.setItem(USER_KEY, JSON.stringify(user));
  }

  private static getUserFromStorage() {
    return JSON.parse(localStorage.getItem(USER_KEY));
  }

  public signOut() {
    window.localStorage.clear();
    this.setUser(null);
  }

  public getCurrentUser() {
    return this.currentUser;
  }

  public setUser(user: User) {
    this.userSource.next(user);
  }

  login(login: any): Observable<any> {
    return this.http.post(API_URL + '/authentication', JSON.stringify(login), httpOptions);
  }

  register(user: any): Observable<any> {
    return this.http.post(API_URL, JSON.stringify(user), httpOptions);
  }

  getUsers(): Observable<any> {
    return this.http.get(API_URL, httpOptions);
  }

  getUser(id: number): Observable<any> {
    return this.http.get(API_URL + `/${id}`, httpOptions);
  }

  updateUserInfo(user: User): Observable<any> {
    const authUser = JSON.parse(window.localStorage.getItem('auth-user'));
    user.token = authUser.token;
    window.localStorage.setItem('auth-user', JSON.stringify(user));
    return this.http.put(API_URL + `/${user.id}`, JSON.stringify(user), httpOptions);
  }

  updateUsersStatus(ids: number[], blocked: boolean) {
    const options = {
      headers: httpOptions.headers,
      params: new HttpParams().set('blocked', String(blocked))
    };
    return this.http.put(API_URL + '/status', JSON.stringify(ids), options);
  }

  updateUsersRole(ids: number[]) {
    const options = {
      headers: httpOptions.headers,
      params: new HttpParams().set('role', Role[Role.ADMIN])
    };
    return this.http.put(API_URL + '/role', JSON.stringify(ids), options);
  }

  deleteUsers(ids: number[]) {
    const options = {
      headers: httpOptions.headers,
      body: JSON.stringify(ids),
    };
    return this.http.delete(API_URL, options);
  }

  public setLanguage(language) {
    localStorage.setItem('language', language);
    this.languageSource.next(language);
  }
}
